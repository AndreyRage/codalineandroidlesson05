package me.codaline.lesson05.activities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import me.codaline.lesson05.R;
import me.codaline.lesson05.database.DbHelper;
import me.codaline.lesson05.entities.User;
import me.codaline.lesson05.entities.User.UserEntry;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private DbHelper mDbHelper;
    private User mUser;
    private Toast mToast;
    private TextView mTextId;
    private EditText mEditFirstName, mEditLastName, mEditAge, mEditAddress;
    private Button mButtonPut, mButtonAdd, mButtonReadFirst, mButtonReadNext, mButtonUpdate, mButtonDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDbHelper = new DbHelper(this);

        DbHelper dbHelper = new DbHelper(this);
        dbHelper.getReadableDatabase();
        dbHelper.getWritableDatabase();

        initScreen();
    }

    private void initScreen() {
        mTextId = (TextView) findViewById(R.id.text_id);

        mEditFirstName = (EditText) findViewById(R.id.edit_first_name);
        mEditLastName = (EditText) findViewById(R.id.edit_last_name);
        mEditAge = (EditText) findViewById(R.id.edit_age);
        mEditAddress = (EditText) findViewById(R.id.edit_address);

        mButtonPut = (Button) findViewById(R.id.button_put);
        mButtonAdd = (Button) findViewById(R.id.button_add);
        mButtonReadFirst = (Button) findViewById(R.id.button_read_first);
        mButtonReadNext = (Button) findViewById(R.id.button_read_next);
        mButtonUpdate = (Button) findViewById(R.id.button_update);
        mButtonDelete = (Button) findViewById(R.id.button_delete);

        mButtonPut.setOnClickListener(this);
        mButtonAdd.setOnClickListener(this);
        mButtonReadFirst.setOnClickListener(this);
        mButtonReadNext.setOnClickListener(this);
        mButtonUpdate.setOnClickListener(this);
        mButtonDelete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add:
                mUser = null;
                showUser();
                break;
            case R.id.button_put:
                if (validate()) {
                    mUser = new User(
                            mEditFirstName.getText().toString(),
                            mEditLastName.getText().toString(),
                            Integer.parseInt(mEditAge.getText().toString()),
                            mEditAddress.getText().toString()
                    );
                    long id = putToDb(mUser);
                    if (id >= 0) {
                        mUser.setId(id);
                        showUser();
                    } else {
                        showToast(R.string.something_went_wrong);
                    }
                }
                break;
            case R.id.button_read_first:
                mUser = readFirstFromDb();
                if (mUser == null) {
                    showToast(R.string.no_records);
                }
                showUser();
                break;
            case R.id.button_read_next:
                User user = readNextFromDb();
                if (user != null) {
                    mUser = user;
                    showUser();
                } else {
                    showToast(R.string.that_is_all);
                }
                break;
            case R.id.button_update:
                if (validate()) {
                    mUser.setFirstName(mEditFirstName.getText().toString());
                    mUser.setLastName(mEditLastName.getText().toString());
                    mUser.setAge(Integer.parseInt(mEditAge.getText().toString()));
                    mUser.setAddress(mEditAddress.getText().toString());
                    updateDbRecord(mUser);
                    showUser();
                }
                break;
            case R.id.button_delete:
                if (mUser != null) {
                    deleteEntry(mUser);
                    mUser = null;
                    showUser();
                }
                break;
        }
    }

    private void showUser() {
        if (mUser != null) {
            mTextId.setText(mUser.getId() != null ? String.format(getString(R.string.id_with_value), mUser.getId()) : "");
            mEditFirstName.setText(mUser.getFirstName() != null ? mUser.getFirstName() : "");
            mEditLastName.setText(mUser.getLastName() != null ? mUser.getLastName() : "");
            mEditAge.setText(String.valueOf(mUser.getAge()));
            mEditAddress.setText(mUser.getAddress()!= null ? mUser.getAddress() : "");

            if (mUser.getId() != null) {
                mButtonPut.setVisibility(View.GONE);
                mButtonAdd.setVisibility(View.VISIBLE);
                mButtonReadNext.setEnabled(true);
                mButtonUpdate.setEnabled(true);
                mButtonDelete.setEnabled(true);
            } else {
                mButtonPut.setVisibility(View.VISIBLE);
                mButtonAdd.setVisibility(View.GONE);
                mButtonReadNext.setEnabled(false);
                mButtonUpdate.setEnabled(false);
                mButtonDelete.setEnabled(false);
            }
        } else {
            mTextId.setText("");
            mEditFirstName.setText("");
            mEditLastName.setText("");
            mEditAge.setText("");
            mEditAddress.setText("");

            mButtonPut.setVisibility(View.VISIBLE);
            mButtonAdd.setVisibility(View.GONE);
            mButtonReadNext.setEnabled(false);
            mButtonUpdate.setEnabled(false);
            mButtonDelete.setEnabled(false);
        }
    }

    private boolean validate() {
        if (TextUtils.isEmpty(mEditFirstName.getText())) {
            showToast(R.string.validate_empty_first_name);
            mEditFirstName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEditLastName.getText())) {
            showToast(R.string.validate_empty_last_name);
            mEditLastName.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEditAge.getText())) {
            showToast(R.string.validate_empty_age);
            mEditAge.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(mEditAddress.getText())) {
            showToast(R.string.validate_empty_address);
            mEditAddress.requestFocus();
            return false;
        }
        return true;
    }

    private void showToast(@StringRes int resString) {
        showToast(getString(resString));
    }

    private void showToast(String string) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(this, string, Toast.LENGTH_LONG);
        mToast.show();
    }

    private long putToDb(User user) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_FIRST_NAME, user.getFirstName());
        values.put(UserEntry.COLUMN_NAME_LAST_NAME, user.getLastName());
        values.put(UserEntry.COLUMN_NAME_AGE, user.getAge());
        values.put(UserEntry.COLUMN_NAME_ADDRESS, user.getAddress());

        // Insert the new row, returning the primary key value of the new row
        return db.insert(
                UserEntry.TABLE_NAME,
                null,
                values
        );
    }

    private User readFirstFromDb() {
        User user = null;

        // Gets the data repository in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                UserEntry._ID,
                UserEntry.COLUMN_NAME_FIRST_NAME,
                UserEntry.COLUMN_NAME_LAST_NAME,
                UserEntry.COLUMN_NAME_AGE,
                UserEntry.COLUMN_NAME_ADDRESS,

        };

        String sortOrder = UserEntry._ID + " ASC";

        Cursor c = db.query(
                UserEntry.TABLE_NAME,   // The table to query
                projection,             // The columns to return
                null,                   // The columns for the WHERE clause
                null,                   // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                String.valueOf(1)       // Limit
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            user = new User();
            user.setId(c.getLong(c.getColumnIndex(UserEntry._ID)));
            user.setFirstName(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_FIRST_NAME)));
            user.setLastName(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_LAST_NAME)));
            user.setAge(c.getInt(c.getColumnIndex(UserEntry.COLUMN_NAME_AGE)));
            user.setAddress(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_ADDRESS)));
        }
        c.close();

        return user;
    }

    private User readNextFromDb() {
        User user = null;

        // Gets the data repository in read mode
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        String[] projection = {
                UserEntry._ID,
                UserEntry.COLUMN_NAME_FIRST_NAME,
                UserEntry.COLUMN_NAME_LAST_NAME,
                UserEntry.COLUMN_NAME_AGE,
                UserEntry.COLUMN_NAME_ADDRESS,

        };

        // Define 'where' part of query.
        String selection = UserEntry._ID + " > ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(mUser.getId()) };

        String sortOrder = UserEntry._ID + " ASC";

        Cursor c = db.query(
                UserEntry.TABLE_NAME,   // The table to query
                projection,             // The columns to return
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder,              // The sort order
                String.valueOf(1)       // Limit
        );

        if (c.getCount() > 0) {
            c.moveToFirst();
            user = new User();
            user.setId(c.getLong(c.getColumnIndex(UserEntry._ID)));
            user.setFirstName(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_FIRST_NAME)));
            user.setLastName(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_LAST_NAME)));
            user.setAge(c.getInt(c.getColumnIndex(UserEntry.COLUMN_NAME_AGE)));
            user.setAddress(c.getString(c.getColumnIndex(UserEntry.COLUMN_NAME_ADDRESS)));
        }
        c.close();

        return user;
    }

    private long updateDbRecord(User user) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // New value
        ContentValues values = new ContentValues();
        values.put(UserEntry.COLUMN_NAME_FIRST_NAME, user.getFirstName());
        values.put(UserEntry.COLUMN_NAME_LAST_NAME, user.getLastName());
        values.put(UserEntry.COLUMN_NAME_AGE, user.getAge());
        values.put(UserEntry.COLUMN_NAME_ADDRESS, user.getAddress());

        // Which row to update, based on the ID
        String selection = UserEntry._ID + " = ?";
        String[] selectionArgs = { String.valueOf(user.getId()) };

        return db.update(
                UserEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
    }

    private long deleteEntry(User user) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Define 'where' part of query.
        String selection = UserEntry._ID + " = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(user.getId()) };

        return db.delete(
                UserEntry.TABLE_NAME,
                selection,
                selectionArgs
        );
    }
}
